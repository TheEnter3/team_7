package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieStringMapper implements Converter<String, List<Long>> {
    @Override
    public List<Long> convert(String from) {
        List<Long> movieIds = new ArrayList<>();
        String[] strings = from.replaceAll(" ", "").split(",");
        for(String number: strings){
            movieIds.add(Long.parseLong(number));
        }
        return movieIds;
    }
}
